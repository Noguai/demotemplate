package demo;

import com.sun.swing.internal.plaf.synth.resources.synth_sv;

import java.util.Scanner;

/**
 * Created by Felix on 20.02.14.
 */
public class Calc {

	public static void main(String[] args) {
		int num1;
		int num2;
		int out = 0;
		char op;
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the first number");
		num1 = scanner.nextInt();
		System.out.println("Enter the second number");
		num2 = scanner.nextInt();
		System.out.println("Enter the operator (+,-)");
		op = scanner.next().charAt(0);
		switch (op) {
			case '+':
				out = num1 + num2;
				break;
			case '-':
				out = num1 - num2;
				break;
			default:
				System.out.println("Don't be silly");
				System.exit(0);
		}
		System.out.println("Result: " + num1 + " " + op + " " + num2 + " = " + out);
	}

}
